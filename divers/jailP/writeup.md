#  JailP 

### Consignes :  


Echappez-vous afin d'obtenir le flag.  
Adresse : adresseduserveur:1234



## Points attribués :  

```
15 Points
```

## Flag :  

```
Fl@g{Esc@p3BasH}
```

### Déroulé :  

Analyse du challenge

Pour se connecter, tapez :


```
nc @chall 1234
```

Il s'agit d'un challenge de type prison. Il faut donc réussir à trouver un moyen de contourner les protections/règles mises en place.
L'idée ici est que chaque commande passe par une regex afin de n'autoriser que certaines lettres, de plus les commandes autorisées ont leurs sorties envoyées vers stderr. Le caractère "/" est également interdit.  

Il faut donc utiliser la commande eval afin d'afficher le flag et rediriger le resultat vers la sortie erreur.  

```
$
eval $( < flag.txt ) 2>&0
./jail.sh: line 18: Fl@g{Esc@p3BasH}: command not found
Commande executee
```
