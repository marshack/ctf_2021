## Garde au mot de passe :


## Consignes :

Notre organisation a réussi à intercepter un échange réseau ennemi, il se pourrait que celui-ci puisse nous permettre de trouver le mot de passe utilisé par un de leurs administrateurs. 

Pourriez-vous regarder ces trames et en extraire le mot de passe du compte "admin" ?


## Points attribués :

```
10 Points
```

## Flag :

```
Fl@g{monkey}
```

## Déroulé :

Dans un premier temps il faut trouver la trame qui réalise l'authentification digest au sein du pcap.
Suite à cela il faut comprendre comment fonctionne le protocol d'authentification DIGEST.

Un indice est donné dans le champs "realm" de la trame d'authentification, sur le fait qu'il va falloir bruteforcer le mot de passe :

```
realm="Protected by Digest, weak passwords allowed"
```

Un script python permet ensuite de calculer les hash MD5 et de valider le mot de passe lorsque le hash calculé correspond au champ "response" de la trame.

L'utilisation d'un dictionnaire est indispensable. Le mot de passe choisi est présent dans tous les dictionnaires que nous avons eu entre les mains.
Pour le writeup nous avons utilisé un extrait du célebre dictionnaire rock you.

Voici le script réalisé :

```python
import hashlib

HA2 = hashlib.md5(b"GET:/admin_folder")

with open('rock_you_truncated.txt', 'r') as dico:
  line = dico.readline().strip()
  while line:
    HA1 = hashlib.md5(b'admin:Protected by Digest, weak passwords allowed:' + line.encode())
    response = hashlib.md5(HA1.hexdigest().encode() + b':5efdce5d5e2997a4:00000001:396dba7bd9d74ce3:auth:' + HA2.hexdigest().encode())

    if  response.hexdigest()=='28af7069628c590202ff338610d247fe':
      print(line)

    line = dico.readline().strip()
```