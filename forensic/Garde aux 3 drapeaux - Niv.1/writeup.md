﻿# Garde aux 3 drapeaux - Niveau 1

## Consignes :

Notre organisation dispose d'un système de transmission point-à-point en HF pour les communications à très longue élongation. Les deux endpoints de ce système sont des serveurs qui font tourner un UNIX sans interface graphique.

Notre station possède un de ces deux endpoints et nous avons reçu récemment un fichier PNG de la part de la station distante. Après avoir transféré l'image PNG sur une machine Windows pour pouvoir l'afficher, nous n'arrivons malheureusement pas à l'ouvrir.

Pourrez-vous nous aider à ouvrir cette image ?

## Points attribués :

```
15 Points
```

## Flag :

```
fl@g{information_confidentielle} ou Fl@g{information_confidentielle}
```

## Déroulé :

Lors du transfert d'un fichier entre UNIX et Windows, certains utilitaires rudimentaires peuvent arbitrairement effectuer la conversion de norme pour les sauts de lignes.

En effet, un saut de ligne est représenté :
- sous UNIX : par \n (0x0a)
- sous Windows : par \r\n (0x0d0a)

Ici, c'est ce qui est arrivé au fichier PNG, ce qui l'empêche évidemment d'être correctement lu.

Pour s'en apercevoir, il existait au moins deux façons :

1. Les premiers octets d'un fichier PNG sont toujours les mêmes (magic numbers).
Ceux sont en hexadécimal : 89:50:4e:47:0d:0a:1a:0a

Ici, le fichier présente l'entête suivante : 89:50:4e:47:0d:0d:0a:1a:0d:0a

On remarque alors que l'octet 0x0a a été remplacé par 0x0d0a.

2. La deuxième façon de détecter le problème était d'utiliser l'utilitaire pngcheck packagé sous Debian, Ubuntu, ...

```
> $ pngcheck -v "Garde aux 3 drapeaux.png"

> File: Garde aux 3 drapeaux.png (1884637 bytes)
> File is CORRUPTED.  It seems to have suffered Unix->DOS conversion.
> ERRORS DETECTED in Garde aux 3 drapeaux.png
```

Le problème est suffisament classique pour que l'utilitaire identifie directement sa cause.

Pour résoudre le problème, nous pouvons soit utiliser la fonction remplacer d'un éditeur héxadécimal, soit scripter cette modification comme le fait le script Python suivant :

```python
with open('Garde aux 3 drapeaux.png', 'rb') as fin:
  with open("Garde aux 3 drapeaux_repaired.png", 'wb') as fout:
    fout.write(fin.read().replace(b"\x0d\x0a", b"\x0a"))
```

Le flag est ensuite lisible directement sur l'image.
