# BiBiPhoque

## Consignes:

Télécharger l'image et récupérer le flag.



## Points attribués

```
50/100
```

## Flag:

```
fl@g{m|o|r|s|e}
```

## Déroulé:


Il faut utiliser stegcracker avec la bibliothèque de mot de passe "rockyou" afin d'extraire le ficher texte qui est protégé par un mot de passe.
```
#apt-get install python3-pip
#pip3.6 install stegcracker

#stegcracker <file> [<wordlist>] 
```

Il faut 15 minutes pour trouver le mot de passe : morse
Ensuite il faut identifier et isoler le flag qui est en binaire :
```
01100110 01101100 01000000 01100111 01111011 00100000 00100000 01111100 00100000 00100000 00100000 01111100 00001001 00100000 00001001 01111100 00001001 00001001 00001001 01111100 00001001 01111101 
```
Par la suite on peut procéder à la conversion en ASCII : 
```
01100110=f
01101100=l
01000000=@
01100111=g
01111011={
00100000=espace
00100000=espace
01111100=|
00100000=espace
00100000=espace
00100000=espace
01111100=|
00001001=tabulation
00100000=espace
00001001=tabulation
01111100=|
00001001=tabulation
00001001=tabulation
00001001=tabulation
01111100=|
00001001=tabulation
01111101=}
```
On obtient donc la chaîne de caractères suivante :  ```fl@g{  |   |	 	|			|	}```

Etant donné que cette dernière ne correspond pas au flag attendu, il faut comprendre que qu'il s'agit d'un mot codé en morse avec la définition suivante (tiret = espace ; point = tabulation).

Ainsi en reprenant la définition ci-dessous on obtient : ```fl@g{--|.--|.-.|...|.}```

Enfin en utilisant la table de correspondance du morse on arrive au flag demandé : ```fl@g{m|o|r|s|e}```



















