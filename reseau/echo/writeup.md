# Écho 

## Consignes :  

Vous avez à disposition une capture réseau simulée (flag.pcap). Trouvez le premier message caché qui vous donnera le moyen de trouver le flag.

## Points attribués :  
```
80 Points
```

Flag :  
```
fl@g{Scapy_est_bon_!}
```

## Déroulé :  

```
Installation de Python 3.8.0

# Optionnel : Pour créer un environnment virtuel
python3 -m venv .venv
source .venv/bin/activate

python3 -m pip install --upgrade pip
python3 -m pip install --upgrade setuptools
python3 -m pip install scapy

python3 ./assets/main_resolve.py
```

Pour résoudre le challenge, il faut dans un premier temps récupérer les paquets `ICMP`, puis les remettre dans l'ordre avec le champ `ID`. <br>
Ensuite, il faut récupérer les données `base64` présentes dans le champ `data` des paquets `ICMP`. <br>
Enfin, il faut décoder et concaténer les données `base64`. Cela  donne une phrase compréhensible indiquant que le flag se trouve dans le padding des `base64`. <br>
Il faut donc récupérer les données binaires présentes dans le padding des `base64`, les concaténer et les convertir en ascii.

Le script commenté ci-dessous permet de résoudre le challenge :

```python
import base64
from scapy.all import *

table_base64 = {'000000': 'A',
                '010001': 'R',
                '100010': 'i',
                '110011': 'z',
                '000001': 'B',
                '010010': 'S',
                '100011': 'j',
                '110100': '0',
                '000010': 'C',
                '010011': 'T',
                '100100': 'k',
                '110101': '1',
                '000011': 'D',
                '010100': 'U',
                '100101': 'l',
                '110110': '2',
                '000100': 'E',
                '010101': 'V',
                '100110': 'm',
                '110111': '3',
                '000101': 'F',
                '010110': 'W',
                '100111': 'n',
                '111000': '4',
                '000110': 'G',
                '010111': 'X',
                '101000': 'o',
                '111001': '5',
                '000111': 'H',
                '011000': 'Y',
                '101001': 'p',
                '111010': '6',
                '001000': 'I',
                '011001': 'Z',
                '101010': 'q',
                '111011': '7',
                '001001': 'J',
                '011010': 'a',
                '101011': 'r',
                '111100': '8',
                '001010': 'K',
                '011011': 'b',
                '101100': 's',
                '111101': '9',
                '001011': 'L',
                '011100': 'c',
                '101101': 't',
                '111110': '+',
                '001100': 'M',
                '011101': 'd',
                '101110': 'u',
                '111111': '/',
                '001101': 'N',
                '011110': 'e',
                '101111': 'v',
                '001110': 'O',
                '011111': 'f',
                '110000': 'w',
                '001111': 'P',
                '100000': 'g',
                '110001': 'x',
                '010000': 'Q',
                '100001': 'h',
                '110010': 'y'}

# Switch des clés / valeurs du dictionnaire
table_base64 = {data_base64:binary_data_base64 for binary_data_base64, data_base64 in table_base64.items()}

def convert_base64_to_bin(base64):

    binary_data_base64 = ''

    for c in base64:

        if c == '=':

            binary_data_base64 += '000000'

        else:

            binary_data_base64 += table_base64[c]

    return binary_data_base64

def group_by_bin_data(data, group_by):

    return [data[i:i + group_by] for i in range(0, len(data) + 1, group_by) if data[i:i + group_by]]

def convert_bin_to_ascii(binary_data):

    return ''.join([chr(int(binary_data[i:i+8], 2)) for i in range(0, len(binary_data) + 1, 8) if binary_data[i:i+8]])

# Changement de couleur de l'affichage des paquets avec Scapy
conf.color_theme = RastaTheme()

# Lecture du fichier PCAP
packets = rdpcap(r'.\binaire\flag.pcap')

dict_data_packets = {}

# Parcours des paquets
for packet in packets:

    # Si le paquet est un paquet ICMP
    if packet.haslayer(ICMP):

        # Si le type du paquet ICMP correspond à un paquet reply
        if packet['ICMP'].type == 0:

            # print(packet['ICMP'].show())

            # Récupération de l'id du paquet et des ses données
            dict_data_packets[packet['ICMP'].id] = packet['Raw'].load.decode()

# Trie de l'ordre des paquets
dict_data_packets = {key:value for key, value in sorted(dict_data_packets.items(), key=lambda x:x[0])}

# print(list(dict_data_packets.values()))

# print(''.join([base64.b64decode(data_packet).decode() for data_packet in dict_data_packets.values()]))

hidden_binary_data = ''

# Parcours des données (encodage base64) des paquets
for data_packet_base64 in dict_data_packets.values():

    # Convertion du base64 en binaire
    binary_data_base64 = convert_base64_to_bin(data_packet_base64)

    # Découpage des données binaires représentant le base64
    # Récupération du deuxième groupe de 6bits
    # Récupération des données binaires du padding modifié
    # Concaténation à chaque tour de boucle des données binaires présentes dans chaque padding de chaque encodage base64
    hidden_binary_data += group_by_bin_data(binary_data_base64, 6)[1][2:]

# Affichage de la conversion des données binaires cachées en ascii
print(convert_bin_to_ascii(hidden_binary_data))
```

