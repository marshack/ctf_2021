
# Hashhashhash

## Consignes :  


Ce système de sauvegarde de données est en vogue mais est-il si infaillible ? 


## Points attribués :  
```
15
```

## Flag :  
```
Fl@g{5e57d93e095d874ffaaeb6a80504fd0e}
```

## Déroulé :  

Vous vous connectez à l'adresse IP:1234

Soit vous prenez l'option i qui vous indique des informations sur votre inscription :

```
Mars@Hack 2020 - Blockchain challenge
Que souhaitez vous faire : inscription (i) / obtention du flag (o)
>i
[ - ] Génération des blocs ...
[ + ] Tâche finie correctement
votre identifiant est l'ID 11 et le seed correspondant 81d5381bc1bd1f795970a22b6f19b0c8
Afin de valider votre inscripton, indiquez la valeur du seed précédent celui qui vient de vous être donné
```

Ou bien sur l'obtention du flag qui indique que seul l'utilisateur 43 peut y accéder.

```

Mars@Hack 2020 - Blockchain challenge
Que souhaitez vous faire : inscription (i) / obtention du flag (o)
>o
[ - ] Génération des blocs ...
[ + ] Tâche finie correctement
Accès reservé à l'utilisateur ID:43 ayant le token af12d03e5b7c2e25ee23f823cd8322ca
Quelle est la valeur du seed précédent ? 

```
Il s'agit d'une hashchain.  
Un seed est défini puis hashé de manière recursive.  
seed -> hash(seed) -> hash(hash(seed)) ...  

Ne connaissant pas le seed, il n'est pas simple de trouver les informations demandées.
Le fait que cela soit du md5 (32 caractères) permet, par bruteforce, de générer toutes les possibilités afin de trouver le flag.

```
#coding:utf8
import md5

liste = {}
seed = "test"


def test():
    seed2 = "81d5381bc1bd1f795970a22b6f19b0c8" #12
    print("Seed: "+seed2);
    found = False;
    hashc2 = seed2;

    while(found == False):
            print(md5.new(hashc2).hexdigest())
            if (md5.new(hashc2).hexdigest() == "be0dc8187703a612fff7926aa58ab054"): 
                    print("Hash found: "+hashc2);
                    found = True;
            else:
                    hashc2 = md5.new(hashc2).hexdigest();

if __name__=="__main__":
    test()

```



```
python exploit.py

python exploit.py 
Seed: 81d5381bc1bd1f795970a22b6f19b0c8
a46ae8d094a190cf890a3d54df54b59c
589293d9e6e3ed1bc91fa2cc828f91d9
e3361486e153b26b36aad2f01e6c90c6
f1902ee7999262719edf2b39a4747f85
a5414c9d27243e857d54b1e2309bf58e
5a6309327e044f0ff51388ac879869db
e45aaef667ac6ae08cf88785c4e901c0
2048561fdd17e80531ba4e29f543ecfc
9edf71b9ce29b59c2db12810f40a2870
9f6b918a4da1dd5a1ef888607aac440d
64c5857d6a56157c426074f3b6437917
f456eb93f6f55ab83146cfb6ce7acc9c
c9cb450e5ccb84806134dbf44e3d3366
9fc4f98751e9f7ad7114b46e6babd3d1
8b40a072809a51b5fbd0f47e95267a08
4c1d5466e3e0dca6adcf8d3379bd5c93
24f80fd406fa88765c631dc78a582230
cca4a1658b11d3bbd905e6f6c4b100b3
808271918ac8d973447dbebe60cc6f77
d49921a2878af306054e20e0c90dc010
9c35a101f93d2999970c06c00639471c
b17f49b533397d68d33732e424d9a5e3
5145e1299684b7f092be73c0795a1509
378d397007693fc72ee20dcab7d04594
31020cc740e8d620b0178171fb175fe0
5f8e518fd288cf9e8492ed4ae9b0382c
9948abfedd979c0407ebae27db4b9e3d
c63501f4aced22d23647445256a5b045
b438911b33e9378fb71391a37f8a1d3c
a7e528b4a79d120bfdd02ab80d2e638f
5e57d93e095d874ffaaeb6a80504fd0e
af12d03e5b7c2e25ee23f823cd8322ca
be0dc8187703a612fff7926aa58ab054
Hash found: af12d03e5b7c2e25ee23f823cd8322ca

```

```
Mars@Hack 2020 - Blockchain challenge
Que souhaitez vous faire : inscription (i) / obtention du flag (o)
>o
[ - ] Génération des blocs ...
[ + ] Tâche finie correctement
Accès reservé à l'utilisateur ID:43 ayant le token af12d03e5b7c2e25ee23f823cd8322ca
Quelle est la valeur du seed précédent ? 
>5e57d93e095d874ffaaeb6a80504fd0e
chaine saisie 5e57d93e095d874ffaaeb6a80504fd0e
le flag est : Fl@g{5e57d93e095d874ffaaeb6a80504fd0e)
```
Fl@g{5e57d93e095d874ffaaeb6a80504fd0e)
