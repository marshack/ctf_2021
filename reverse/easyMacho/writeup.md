#  EasyMacho / Baptiste

## Consignes :  


Un binaire extrait d'un mac demande un mot de passe à l'exécution. Le Calcul le plus simple donnera le flag !

## Points attribués :  

```
10 Points
```

## Flag :  

Plusieurs flags sont possibles

```
fl@g{DDHMB@}
fl@g{DDHMBA}
fl@g{DDHMBB}
fl@g{DDHMBC}
fl@g{DDHMBD}
fl@g{DDHMBE}
fl@g{DDHMBF}
fl@g{DDHMBG}
```

Vu l'indice dans les consignes et vu la difficulté du challenge il serait intéressant de restreindre au seul premier flag.


## Déroulé :  


Le challenge a été développé en C et compilé sur MacOS.

Prenons des informations sur le binaire : 

```
$ file macho-very-easy
macho-very-easy: Mach-O 64-bit executable x86_64
```
Le binaire est un format mach-o, de manière simple il est donc exécutable que sur MacOS. Considant qu'il n'est pas courant d'avoir un Mac, l'objectif du challenge doit être de faire l'analyse de manière statique.

A l'aide de radare2 ou un autre outil, il est possible d'obtenir l'affichage suivant : 

<img src="assets/CFG-full.png" />

La résolution des fonctions permet de comprendre, qu'il y a une saisie `scanf` suivie de plusieurs comparaisons `cmp`. Si la comparaison est vraie, on passe à la comparaison suivante jusqu'à l'affichage de la chaine "Gooood !!!!" qui signifie la réussite du challenge. En regardant attentivement après la fonction `scanf`, on se rend compte que la chaine saisie est stockée dans la variable pointée par `[rbp+var_10]`. Il convient ensuite d'analyser les blocs suivants.

### Premier bloc :

```nasm
mov     rsi, [rbp+var_10]   ; l'adresse de la chaine saisie est mis dans RSI
movsx   ecx, byte ptr [rsi] ; le premier caractère de la chaine saisie est mis dans ECX 
movsx   edx, [rbp+var_11]   ; [rbp+var_11] (soit 0x3) est mise dans EDX 
add     edx, 41h ; 'A'      ; EDX = 0x41 + EDX où EDX vaut 3. Soit EDX = 0x44
cmp     ecx, edx            ; on compare EDX avec ECX
mov     [rbp+var_28], eax   ; 
jnz     loc_100000F0A       ; Si ECX <> EDX alors Affichage de l'échec et fin du programme
```

On comprend alors que le premier caractère doit être égal à 0x44 soit 'D'.

### Deuxième bloc :

```nasm
mov     rax, [rbp+var_10]       ; idem bloc 1
movsx   ecx, byte ptr [rax+1]   ; le 2ème caractère est mis dans ECX 
mov     edx, [rbp+var_18]       ; 0x112 est mis dans edx
sar     edx, 2                  ; 0x112 >> 2 soit 0x44
cmp     ecx, edx                ; on compare EDX avec ECX
jnz     loc_100000F05           ; Si ECX <> EDX alors Affichage de l'échec et fin du programme
```

On comprend alors que le second caractère doit être aussi égal à 0x44 soit 'D'.

### Troisième bloc :

```nasm
mov     rax, [rbp+var_10]       ; idem bloc 1
movsx   ecx, byte ptr [rax+2]   ; le 3ème caractère est mis dans ECX 
mov     edx, [rbp+var_1C]       ; 0x1233 est mis dans edx
sar     edx, 6                  ; 0x1233 >> 6 soit 0x48 
cmp     ecx, edx                ; on compare EDX avec ECX
jnz     loc_100000F00           ; Si ECX <> EDX alors Affichage de l'échec et fin du programme
```

On comprend alors que le troisième caractère doit être égal à 0x48 soit 'H'.

### Quatrième bloc :

```nasm
mov     rax, [rbp+var_10]       ; idem bloc 1
movsx   ecx, byte ptr [rax+3]   ; le 4ème caractère est mis dans ECX 
mov     eax, [rbp+var_20]       ; 0x3C28 est mis dans eax
cdq                             ; Conversion de taille de mot, pas important ici
mov     esi, 0C8h               ; 0xC8 est mis dans ESI
idiv    esi                     ; on effectue EAX/ESI soit 0x3C28/OxC8=0x44
cmp     ecx, edx                ; on compare EDX avec EDX
jnz     loc_100000EFB           ; Si ECX <> EAX alors Affichage de l'échec et fin du programme
```

On comprend alors que le quatrième caractère doit être égal à 0x4D soit 'M'.

### Cinquième bloc :

```nasm
mov     rax, [rbp+var_10]       ; idem bloc 1
movsx   ecx, byte ptr [rax+4]   ; le 5ème caractère est mis dans ECX
shl     ecx, 3                  ; ECX=ECX << 3
cmp     ecx, 210h               ; On compare ECX avec 0x210
jnz     loc_100000EF6           ; Si ECX <> 0x210 alors Affichage de l'échec et fin du programme
```

On comprend alors que ECX << 3 doit être égal à 0x210, les propriétés de l'opération "<<" permettent de réaliser l'opération inverse avec ">>", aussi le cinquième caractère doit être égal à 0x210 >> 3 = 0x42 soit 'B'.

### Sixième bloc :

```nasm
mov     rax, [rbp+var_10]       ; idem bloc 1
movsx   ecx, byte ptr [rax+5]   ; le 6ème caractère est mis dans ECX
sar     ecx, 3                  ; ECX=ECX >> 3
cmp     ecx, 8                  ; On compare ECX avec 0x8
jnz     loc_100000EF1           ; Si ECX <> 0x8 alors Affichage de l'échec et fin du programme
```

On comprend alors que ECX >> 3 doit être égal à 0x8, les propriétés de l'opération ">>" permettent de réaliser l'opération inverse avec "<<", aussi le sixième caractère doit être égal à 0x8 << 3 = 0x40 soit '@'.

