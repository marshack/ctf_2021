import hashlib

HA2 = hashlib.md5(b"GET:/admin_folder")

with open('rock_you_truncated.txt', 'r') as dico:
  line = dico.readline().strip()
  while line:
    HA1 = hashlib.md5(b'admin:Protected by Digest, weak passwords allowed:' + line.encode())
    response = hashlib.md5(HA1.hexdigest().encode() + b':5efdce5d5e2997a4:00000001:396dba7bd9d74ce3:auth:' + HA2.hexdigest().encode())

    if  response.hexdigest()=='28af7069628c590202ff338610d247fe':
      print(line)

    line = dico.readline().strip()
