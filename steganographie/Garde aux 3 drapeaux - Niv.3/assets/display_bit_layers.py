from PIL import Image, ImageDraw

# Extract first bit layer
img = Image.open('Garde aux 3 drapeaux_repaired_and_filled.png')
img = img.convert("RGB")
draw = ImageDraw.Draw(img)

for y in range(img.height):
  print("\rLines processed : {} / {}".format(y+1, img.height), end='')
  for x in range(img.width):
    px = img.getpixel((x, y))
    px = tuple((i & 0b1) * 255 for i in px)
    draw.point((x,y), px)

print()

img.save('first bit layer.png', 'PNG')

#Extract second bit layer
img = Image.open('Garde aux 3 drapeaux_repaired_and_filled.png')
img = img.convert("RGB")
draw = ImageDraw.Draw(img)

for y in range(img.height):
  print("\rLines processed : {} / {}".format(y+1, img.height), end='')
  for x in range(img.width):
    px = img.getpixel((x, y))
    px = tuple(((i & 0b10) >> 1) * 255 for i in px)
    draw.point((x,y), px)

print()

img.save('second bit layer.png', 'PNG')
