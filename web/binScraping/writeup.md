# BinScraping

## Consignes :  

Des données binaires se cachent parmi les pages de ce "mini" site web.
A vous de réorganiser l'ensemble des données que vous trouverez de façon à les rendre compréhensibles pour trouver le flag.

## Points attribués :  
```
40 Points
```

## Flag :  
```
fl@g{Django_Scraping_Mars@Hack}
```

## Déroulé :  

Résolution du challenge

```
sudo apt-get install python3.8

python3 -m venv .venv
source .venv/bin/activate
python3 -m pip install --upgrade pip
python3 -m pip install --upgrade setuptools
python3 -m pip install bs4
python3 -m pip install requests

python3 ./assets/main_resolve.py
```

Plusieurs liens sont accessibles à la racine du site web `http://{@IP}:8000/`. <br>
Les liens ne changent pas mais sont mélangés de manière aléatoire à chaque rechargement de page. Des liens ne menant nulle part sont aussi présents. <br>
Le but est d'automatiser la récupération des données présentes dans chaque lien. <br>
Il faut donc parser avec le module `BeautifulSoup` la page d'index HTML et récupérer l'ensemble des liens hormis ceux ne menant nulle part. <br>
Ensuite, il faut effectuer des requêtes GET avec le module `Requests` sur les liens afin de récupérer le contenu des pages web qui s'avère être du JSON. <br>
Travailler sur l'ensemble des JSON pour écarter ceux avec des données `null`, trier les données par ordre de création (timestamp), concaténer les données binaires et les convertir en ascii. 




