# StrongWall - Level 1

## Informations sur le challenge


<table>
<tr>
    <td>Nom de l'épreuve</td>
    <td>StrongWall - Level 1</td>
</tr>
<tr>
    <td>Créateur</td>
    <td>RavenXploit (Romain ORTEGA)</td>
</tr>
<tr>
    <td>Niveau de l'épreuve</td>
    <td>1/3</td>
</tr>
<tr>
    <td>Catégorie de l'épreuve</td>
    <td>Web</td>
</tr>
<tr>
    <td>Points attribués</td>
    <td>TODO</td>
</tr>
</table>


## Consignes :
```
Ce challenge représente le premier niveau, sur trois, du challenge StrongWall.
Le niveau suivant ne peut-être débloqué tant que le précédent n'a pas été résolu.

Ce premier niveau de StrongWall est une solution exotique visant à protéger la page d'authentification d'une application.

Le flag est de la forme fl@g{}

Bonne chance !

```

## Flag :

```
fl@g{N3v3rD3pl0y3x0ticS3curity}
```


## Architecture :  

```
Le niveau 1 de StrongWall est basé sur du web.
Le serveur est dévelloppé en Go et s'execute sur le port 8080 par défaut.

Le déploiement se fait via docker.

```