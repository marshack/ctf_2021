# Back to Basic

## Consignes :

Vos compétences multiples ont fait parler de vous, l'officier en charge de la mission vous tend la lettre suivante.

```tex
========================================   CONFIDENTIEL  ========================================

Monsieur le Prefet,

  J'ai l'honneur de vous rendre compte de l'incident qui s'est produit le 14/02/2020 à 15h15, lorsque mon équipe de démolition procédait à la destruction du batiment HC45, comme stipulé dans notre ordre d'intervention.

  En effet, après avoir terminé la partie Est du batiment, nous avons mis à jour une pièce en contrebas. Sécurisant les alentours, nous sommes descendus constater l'intégrité structurelle, afin de poursuivre nos travaux. Quelle ne fut pas notre surprise, de découvrir une grosse structure en béton, avec une lourde porte en acier et un vieux terminal monochrome, surmonté d'un large panneau arborant le texte suivant : "ABRI 101".

  Nous avons interompu immédiatement les travaux et informé les autorités de notre découverte.
  
  														Respectueusement,
  														M. Vault, Entreprise BTP
  														
========================================   CONFIDENTIEL  ========================================
```

Après quelques recherches dans les archives militaires, en attendant votre arrivée, il a pu mettre la main sur la sauvegarde de l'algorithme d'encodage du fichier de clé :

- le fichier à décoder : **abri101.enc.bin**
- Le fichier **algorithm.bas** qui a servi à le créer

Une fois le fichier décodé, il révelera les caractères nécessaires pour ouvrir la porte en acier.

Mettez vos compétences à profit pour trouver le Clair, qui validera le challenge.



## Points attribués

```
30 Points
```



## Flag

```
fl@g{R3m3mb3R_Y3ar5_Ag0_B@cK_t0_8a5iC}
```

## Déroulé

```
- Analyse code source BASIC.
- Inversion du code pour obtenir le Clair à partir du fichier encodé.
```

## Résolution du challenge "abri101.enc.bin" en BASIC

- Décompresser le fichier **algorithm.zip**.
- Inverser la logique du programme **algorithm.bas**. Explications :

```basic
' Verification de l'existence du fichier abri101.key
IF NOT _FILEEXISTS("abri101.key") THEN
    COLOR 11
    PRINT "abri101.key : File not found."
ELSE

    ' Si le fichier abri101.key existe on va l'ouvrir en lecture
    OPEN "abri101.key" FOR INPUT AS #1
    ' On récupère son contenu dans la variabe Text$
    IF LOF(1) <= 32000 THEN Text$ = INPUT$(LOF(1), 1)
        ' On ferme le fichier une fois lu entierement
    CLOSE #1
        
    ' Initialisation des variables 
    Send$ = ""
    binary$ = ""
    ' SaveCode est notre variable de stockage après chaque traitement
    ' Elle contient le chiffre qui correspond à notre lettre
    SaveCode = 67
    ' On boucle sur la longueur du texte
    FOR i = 1 TO LEN(Text$)
    	' On stock chaque lettre dans la variable Letter$
        Letter$ = MID$(Text$, i, 1)
    	' On stock le code ASCII de la lettre dans la variable Code
        Code = ASC(Letter$)
        ' On ne récupère que les charactères ASCII entre 48 et 125 inclus
        ' c'est a dire : 0-9 :;<=>?@ A-Z [\]^_` a-z {|}
        ' de quoi faire un beau flag
        IF (Code > 47 AND Code < 126) THEN
        	' On décalle le Code ASCII de 125 - le code de la lettre précédente
        	' On stock ce calcul dans la variable byte 
            byte = Code + 125 - SaveCode
            ' On calcule le code binaire de notre valeur byte
            FOR bit = 7 TO 0 STEP -1
                IF byte AND 2 ^ bit THEN
                    binary$ = binary$ + "1"
                ELSE
                    binary$ = binary$ + "0"
                END IF
            NEXT
            ' binary$ contient maintenant notre lettre encodée sous forme binaire
            ' et à chaque itération contient une lettre de plus
            ' On sauvegarde le code ASCII de la lettre originale pour la boucle suivante
            SaveCode = Code
        END IF
    NEXT i

	' On creer un fichier de sortie avec notre chaine binaire
    OPEN "abri101.enc.bin" FOR OUTPUT AS #2
    PRINT #2, binary$
    CLOSE #2

END IF

```

- [Télécharger QBasic64](https://github.com/QB64Team/qb64/releases/download/v1.4/qb64_1.4_win_x64.zip)
- Compiler le programme **resolve_bin.bas** qui contient la solution, explication :

```basic
' Vérification de l'existence du fichier abri101.enc.bin
IF NOT _FILEEXISTS("abri101.enc.bin") THEN
    COLOR 11
    PRINT "abri101.enc.bin : File not found."
ELSE
    ' Lecture du fichier abri101.enc.bin dans la variable binary$
    OPEN "abri101.enc.bin" FOR INPUT AS #1
    binary$ = INPUT$(LOF(1), 1)
    CLOSE #1

    ' On affiche la chaine lue dans le fichier
    PRINT "************** abri101.enc.bin ****************"
    PRINT binary$

    ' On initialise comme dans le programme d'encodage ( surtout SaveCode )
    decoded_binary$ = ""
    SaveCode = 67
    MCode = 0
    ' On boucle sur l'ensemble de la chaine binaire par step de 8
    FOR i = 1 TO LEN(binary$) STEP 8
    ' bintoconvert$ contient les 8 charactères
        bintoconvert$ = MID$(binary$, i, 8)
    	' On initialise number% qui sera notre chiffre ASCII pendant calcul
        number% = 0
	    ' On boucle sur chaque caractère
        FOR j = 1 TO 8
		   ' On stock la valeur dans temp$
            temp$ = MID$(bintoconvert$, j, 1)
		    ' On calcul à la main la valeur en foncion des 0 et des 1
		    ' Aucune fonction n'existe en basic pour le faire
            IF (temp$ = "1" AND j = 1) THEN number% = number% + 128
            IF (temp$ = "1" AND j = 2) THEN number% = number% + 64
            IF (temp$ = "1" AND j = 3) THEN number% = number% + 32
            IF (temp$ = "1" AND j = 4) THEN number% = number% + 16
            IF (temp$ = "1" AND j = 5) THEN number% = number% + 8
            IF (temp$ = "1" AND j = 6) THEN number% = number% + 4
            IF (temp$ = "1" AND j = 7) THEN number% = number% + 2
            IF (temp$ = "1" AND j = 8) THEN number% = number% + 1
        NEXT j
        ' Code va contenir notre code ASCII resultant encodé  
        Code = number%
        ' MCode va stocker notre code ASCII decodé
        ' Logique d'encodage pour rappel :
        ' byte = Code + 125 - SaveCode
        ' Nous devons donc retirer 125 et ajouter SaveCode
        MCode = Code + SaveCode - 125
        ' Lettre$ contient le caractère decodé de notre chaine
        Letter$ = CHR$(MCode)
        ' decoded_binary$ contiendra toutes les lettres de notre Flag
        decoded_binary$ = decoded_binary$ + Letter$
        ' On sauvegarde le code ASCII de notre lettre décodée
        SaveCode = MCode
    NEXT i

    ' Affichage du résultat
    PRINT "************** Decoded Binary FLAG ****************"
    PRINT decoded_binary$

    ' Eventuellement écriture du resultat dans un fichier
    OPEN "abri101.flag.txt" FOR OUTPUT AS #2
    PRINT #2, decoded_binary$
    CLOSE #2
END IF
```

- lancer **resolve_bin.exe** avec **abri101.enc.bin** dans le même dossier.
- On obtient un fichier **abri101.flag.txt** mais le flag est aussi affiché à l'écran.



## Résolution du challenge "abri101.enc.bin" en PYTHON

```python
#!/usr/bin/env python3
import binascii

def text_from_bits(bits, encoding='utf-8', errors='surrogatepass'):
    n = int(bits, 2)
    return int2bytes(n)

def int2bytes(i):
    hex_string = '%x' % i
    n = len(hex_string)
    return binascii.unhexlify(hex_string.zfill(n + (n & 1)))


ciphertext="1010000010000011010100011010010010010001010101000101111010110111010000111011011101110010010011101001110010001010011101110101011110101011100011100100000010100111010111111010001101000110101011000110000001111011101000000110010110010001100100100011100110101100010101101010011001010001101100010101011110110111"


# Defining splitting point 
n = 8
maliste  = [(ciphertext[i:i+n]) for i in range(0, len(ciphertext), n)]

cleartext = ""
savecode = 67
for car in maliste:

 res = (binascii.hexlify(text_from_bits(car)))
 res = int(res,16) + savecode - 125
 cleartext = cleartext + (chr(res))
 savecode =res

print (cleartext)
```
