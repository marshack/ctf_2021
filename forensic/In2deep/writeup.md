# in2deep

## Consignes :  

Un des collaborateurs de l'évènement a cassé son MacBook, lequel contenait l'épreuve qu'il avait réalisée sous forme de conteneur grâce à Docker Desktop for Mac. Après avoir extrait le contenu du SSD, il a pu récupérer le fichier `Dockerfile` de l'épreuve ainsi que les données de la machine virtuelle Docker, mais ne sait pas quoi en faire. À toi de retrouver le flag !


## Points attribués :  
```
40 Points
```

## Flag :  
```
Fl@g{d0ck3r_4n6_is_k3wl}
```

## Déroulé : 

Cette solution prend le parti de n'utiliser que les outils GNU/Linux ainsi que ceux proposés par la suite The Sleuth Kit (TSK).

### Exploitation du fichier de construction

Le fichier `Dockerfile` étant directement exploitable, on constate notamment que :

* L'image comporte 5 couches distinctes (on peut ignorer les mots-clés `ENTRYPOINT` et `ARG` qui n'affectent pas le système de fichiers) ;
* Le drapeau est déposé au sein même de l'image, comme indiqué par l'utilisation de la commande `COPY` ;
* L'extension `.enc` de ce dernier et l'utilisation de la suite OpenSSL indiquent que le drapeau est probablement chiffré ;
* Ce dernier est déchiffré lors de l'exécution du conteneur grâce à la suite OpenSSL (commande `openssl` balisée par le mot-clé `ENTRYPOINT`) ;
* Le mot de passe permettant de dériver la clé de chiffrement est passé au conteneur via la variable d'environnement `RT_FLAG_PASSWORD` lors de l'instanciation ;
* On connait d'emblée l'algorithme de chiffrement (AES), la taille de la clé (256 bits), le mode d'opération (CBC) ainsi que l'algorithme de hachage (SHA-512) ayant permis de chiffrer le drapeau.

### Identification de l'image

On s'attarde ensuite sur le fichier compressé `in2deep.raw.xz`.

Tout d'abord, on décompresse ce dernier via la commande `xz -d -T 0 in2deep.raw.xz`, ce qui créé un fichier `in2deep.raw`.

Avant d'aller plus loin, il est nécessaire de savoir à quel type de fichier on a affaire. Pour cela, on peut dans un premier temps utiliser la commande `file in2deep.raw` pour tenter d'identifier d'éventuelles suites d'octets connues :

```sh
$ file in2deep.raw

in2deep.raw: DOS/MBR boot sector; partition 1 : ID=0x83, active, start-CHS (0x0,32,33), end-CHS (0x399,31,6), startsector 2048, 31246336 sectors
```

On constate que le fichier contient un secteur d'amorçage MBR, ce qui concorde avec l'énoncé de l'épreuve.

Pour les non-initiés, une recherche sur Internet permet de comprendre que Docker tire parti de fonctionnalités proposées par le noyau Linux, lequel n'est pas présent nativement sous macOS, ce qui explique que Docker nécéssite l'utilisation d'une machine virtuelle en arrière-plan (dans le cas présent une machine légère tournant grâce à xhyve).

### Montage de la partition

On tente à présent de lister les partitions contenues sur le disque virtuel grâce à la commande `mmls in2deep.raw` :

```sh
$ mmls in2deep.raw

DOS Partition Table
Offset Sector: 0
Units are in 512-byte sectors

      Slot      Start        End          Length       Description
000:  Meta      0000000000   0000000000   0000000001   Primary Table (#0)
001:  -------   0000000000   0000002047   0000002048   Unallocated
002:  000:000   0000002048   0031248383   0031246336   Linux (0x83)
```

On note tout particulièrement :

* Qu'une partition `Linux (0x83)` est présente à l'index 2048 ;
* Que la taille d'un bloc correspond à 512 octets.

On tente ensuite de déterminer le type de système de fichiers utilisé pour cette partition via la commande `fsstat -o 2048 -b 512 in2deep.raw`. Le but étant de pouvoir ensuite la monter sans encombres.

```sh
$ fsstat -o 2048 -b 512 in2deep.raw

FILE SYSTEM INFORMATION
--------------------------------------------
File System Type: Ext4
Volume Name:
Volume ID: 50002c37a01a67b7744207a75deda543

Last Written at: 2020-01-18 17:23:01 (UTC)
Last Checked at: 2020-01-18 17:22:58 (UTC)

Last Mounted at: 2020-01-18 17:23:01 (UTC)
Unmounted properly
Last mounted on: /var/lib
[...]
```

On constate que le système de fichiers utilisé est ext4. On peut donc monter la partition sereinement via la commande `mount -t ext4 -o ro,noload,offset=1048576 in2deep.raw /mnt/docker`. L'index 1048576 correspond au décalage de la partition en question, multiplié par le taille d'un secteur (information récoltée précédemment).

Le seul répertoire de l'image pertinent pour l'épreuve est le répertoire `/mnt/docker/docker` (hormis les journaux d'évènements situés dans `/mnt/docker/log` qui apportent tout de même quelques informations). Le reste de l'image ne concerne que la machine Docker en elle-même.

On déduit par la présence des répertoires `/mnt/docker/docker/overlay2` et `/mnt/docker/docker/image/overlay2` que le système de fichiers utilisé intrinsèquement par Docker est OverlayFS.

### Récupération du mot de passe

En navigant dans le répertoire `/mnt/docker/docker/containers`, on note la présence d'un seul et unique conteneur identifié `5a993056a7faab2545257f58ac1611edb6303deb0caadd1ec7a4274cf5cebc97`.

On peut afficher les métadonnées associées à ce dernier via la commande `jq . /mnt/docker/docker/containers/5a993056a7faab2545257f58ac1611edb6303deb0caadd1ec7a4274cf5cebc97/config.v2.json` :

```json
$ jq . /mnt/docker/docker/containers/5a993056a7faab2545257f58ac1611edb6303deb0caadd1ec7a4274cf5cebc97/config.v2.json

{
  "StreamConfig": {},
  "State": {
    "Running": false,
    "Paused": false,
    "Restarting": false,
    "OOMKilled": false,
    "RemovalInProgress": false,
    "Dead": false,
    "Pid": 0,
    "ExitCode": 0,
    "Error": "",
    "StartedAt": "2020-01-18T18:36:51.317164853Z",
    "FinishedAt": "2020-01-18T18:36:51.399201938Z",
    "Health": null
  },
  "ID": "5a993056a7faab2545257f58ac1611edb6303deb0caadd1ec7a4274cf5cebc97",
  "Created": "2020-01-18T18:36:50.63714395Z",
  "Managed": false,
  "Path": "/bin/sh",
  "Args": [
    "-c",
    "openssl enc -d -aes-256-cbc -nosalt -pbkdf2 -md sha512 -pass pass:$RT_FLAG_PASSWORD -in $RT_FLAG_PATH_INSIDE_THE_IMAGE > /too/bad/no/logs && echo \"Tout fonctionne !\""
  ],
  "Config": {
    "Hostname": "5a993056a7fa",
    "Domainname": "",
    "User": "",
    "AttachStdin": false,
    "AttachStdout": true,
    "AttachStderr": true,
    "Tty": false,
    "OpenStdin": false,
    "StdinOnce": false,
    "Env": [
      "RT_FLAG_PATH_INSIDE_THE_IMAGE=/tmp/2611da6036d2dabff02da9a8be81dd5c2ad67c655d146ac8271c049f514c417e",
      "RT_FLAG_PASSWORD=m0ms_sp4ghett1",
      "PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin"
    ],
    "Cmd": null,
    "Image": "marsathack2/in2deep:latest",
    "Volumes": null,
    "WorkingDir": "",
    "Entrypoint": [
      "/bin/sh",
      "-c",
      "openssl enc -d -aes-256-cbc -nosalt -pbkdf2 -md sha512 -pass pass:$RT_FLAG_PASSWORD -in $RT_FLAG_PATH_INSIDE_THE_IMAGE > /too/bad/no/logs && echo \"Tout fonctionne !\""
    ],
    "OnBuild": null,
    "Labels": {}
  },
  "Image": "sha256:362a13ddca24f0da83178a6ef62b4e54a3fdbe7608d4861a33a7fb8204cb4682",
  [...]
  "Name": "/this_is_the_way",
  "Driver": "overlay2",
  [...]
}
```

De là, on comprend que :

* Le conteneur n'était pas en fonctionnement pas lors du prélèvement (cf. clé `Running: false`) ;
* L'exécution du conteneur en question s'est achevée sans erreur (cf. clé `ExitCode: 0`).

Grâce au contexte fourni par le fichier `Dockerfile` et au code de retour du conteneur, on en déduit que le drapeau a été correctement déchiffré, et par conséquent que le mot de passe fourni via la variable `RT_FLAG_PASSWORD` était le bon.

On prend également note des données suivantes pour la suite :

* Identifiant du conteneur : `5a993056a7faab2545257f58ac1611edb6303deb0caadd1ec7a4274cf5cebc97` ;
* Identifiant de l'image ayant servie de base à ce dernier : `sha256:362a13ddca24f0da83178a6ef62b4e54a3fdbe7608d4861a33a7fb8204cb4682`.
* Variable d'environnement `RT_FLAG_PATH_INSIDE_THE_IMAGE` : `/tmp/2611da6036d2dabff02da9a8be81dd5c2ad67c655d146ac8271c049f514c417e` ;
* Variable d'environnement `RT_FLAG_PASSWORD` : `m0ms_sp4ghett1`.

En affichant le contenu du magasin d'images à l'aide de la commande `jq . /mnt/docker/docker/image/overlay2/repositories.json`, on peut confirmer que le nom assigné à cette image a bien un lien avec l'épreuve :

```json
$ jq . /mnt/docker/docker/image/overlay2/repositories.json

{
  "Repositories": {
    "alpine": {
      [...]
    },
    "marsathack2/in2deep": {
      "marsathack2/in2deep:latest": "sha256:362a13ddca24f0da83178a6ef62b4e54a3fdbe7608d4861a33a7fb8204cb4682"
    },
    "random.kontain.me/random": {
      [...]
    }
  }
}
```

### Tracé de la chaîne de couches successives

Une fois l'identifiant du conteneur en main, on peut remonter la chaîne des couches qui la composent.

Si l'on se fie au fichier `Dockerfile` ayant servi à créer l'image, on constate que l'opération de copie du drapeau est effectuée au niveau de la troisième couche. C'est donc cette dernière que l'on va vouloir explorer en priorité.

Avant de pouvoir consulter les données contenues dans les couches, il faut récupérer l'identifiant du cache (emplacement où sont stockées les données "affectant" les couches en lecture seule) correspondant à la couche que l'on souhaite explorer. Cette information est contenue dans le fichier `cache-id`, situé dans les répertoires propres aux couches (ex. `/mnt/docker/docker/image/overlay2/layerdb/sha256/6faa4901499bbcb48215a519a792d163b0405626eb3adf3a9f204ffc4c15d1e7/cache-id`).

Cependant, il faut tout d'abord trouver un moyen de remonter la chaîne jusqu'à la troisième couche. Le fichier `parent` présent dans le répertoire de chaque couche (ex. `/mnt/docker/docker/image/overlay2/layerdb/sha256/6faa4901499bbcb48215a519a792d163b0405626eb3adf3a9f204ffc4c15d1e7/parent`) permet de connaître l'identifiant de la couche parente.

On remonte donc la chaîne de cette façon, en partant de l'identifiant du conteneur (qui est également l'identifiant de la couche de surface).

```sh
$ cat /mnt/docker/docker/image/overlay2/layerdb/mounts/5a993056a7faab2545257f58ac1611edb6303deb0caadd1ec7a4274cf5cebc97/parent

sha256:1571215d88e1e52c182be366b3118059338d5626155467a6f5dae333f1f44555
```

Cette couche étant propre au conteneur (en lecture/écriture), elle se trouve dans un répertoire différent (en l'occurence `mount`). Le reste des couches en lecture seule se trouve dans le répertoire `/mnt/docker/docker/image/overlay2/layerdb/sha256`.

On retrouve ensuite l'identifiant de la couche précédente :

```sh
$ cat /mnt/docker/docker/image/overlay2/layerdb/sha256/1571215d88e1e52c182be366b3118059338d5626155467a6f5dae333f1f44555/parent

sha256:61f0bb023dc3e1ab6e9ee949f45226cd75f410592fab0b53428495679674096f
```

On continue de remonter la chaîne...

```sh
$ cat /mnt/docker/docker/image/overlay2/layerdb/sha256/61f0bb023dc3e1ab6e9ee949f45226cd75f410592fab0b53428495679674096f/parent

sha256:8161df60468948b9a78babaf2e7324568ae48540ca214cd1005147494e536053
```

Une fois l'identifiant de la couche en notre possession, il est nécéssaire de récupérer l'identifiant du cache correspondant, qui contient le différentiel appliqué au système de fichiers par rapport à la couche parente.

On peut récupérer cet identifiant dans le fichier `cache-id` :

```sh
$ cat /mnt/docker/docker/image/overlay2/layerdb/sha256/8161df60468948b9a78babaf2e7324568ae48540ca214cd1005147494e536053/cache-id

68be51690133ef62d2b8d91af2916c8ad263effeca100f4a6380480369ecb468
```

En suivant cette méthodologie, on arrive finalement à reconstruire la chaîne suivante :

* [rw] `5a993056a7faab2545257f58ac1611edb6303deb0caadd1ec7a4274cf5cebc97` (pas de cache car la couche est en lecture/écriture)
* [ro] `1571215d88e1e52c182be366b3118059338d5626155467a6f5dae333f1f44555` [cache] `dd7e537e7d226334b35d6a8250a62b79c44baccff56721af4276dc024403ad54`
* [ro] `61f0bb023dc3e1ab6e9ee949f45226cd75f410592fab0b53428495679674096f` [cache] `7b3870b9cbbe629636ded7c384c21c52d150fb51f4693b431f40098118f6fb53`
* [ro] `8161df60468948b9a78babaf2e7324568ae48540ca214cd1005147494e536053` [cache] `68be51690133ef62d2b8d91af2916c8ad263effeca100f4a6380480369ecb468`
* [ro] `6faa4901499bbcb48215a519a792d163b0405626eb3adf3a9f204ffc4c15d1e7` [cache] `b1f5fa2babc00ee9881d4cbf303a59b1bbb85856169223053ecbc637b63f1c05`
* [ro] `5216338b40a7b96416b8b9858974bbe4acc3096ee60acbc4dfb1ee02aecceb10` [cache] `81a8d99a40168013e4a71c339654cb039b018c0d4928f6a719a1ff262a492749`

En listant les fichiers présents dans ce cache, on aperçoit le drapeau `2468e4d24a731051e23b87de9a6ec14d9d3a886b5d9b8bd779fa325a2b9abc53` à l'intérieur du répertoire `diff` :

```sh
$ tree /mnt/docker/docker/overlay2/68be51690133ef62d2b8d91af2916c8ad263effeca100f4a6380480369ecb468

/mnt/docker/docker/overlay2/68be51690133ef62d2b8d91af2916c8ad263effeca100f4a6380480369ecb468
|-- committed
|-- diff
|   `-- tmp
|       `-- 2611da6036d2dabff02da9a8be81dd5c2ad67c655d146ac8271c049f514c417e
|-- link
|-- lower
`-- work
```

### Déchiffrement du drapeau

AES étant un algorithme de chiffrement symétrique, il est possible de déchiffrer une donnée à condition d'utiliser la même clé et le même mode d'opération (dans le cas présent CBC).

On peut donc finalement déchiffrer le drapeau à l'aide du mot de passe récupéré à l'étape 1 en exécutant la commande suivante :

```sh
$ cat /mnt/docker/docker/overlay2/68be51690133ef62d2b8d91af2916c8ad263effeca100f4a6380480369ecb468/diff/tmp/2611da6036d2dabff02da9a8be81dd5c2ad67c655d146ac8271c049f514c417e | openssl enc -d -aes-256-cbc -nosalt -pbkdf2 -md sha512 -pass pass:m0ms_sp4ghett1

Fl@g{d0ck3r_4n6_is_k3wl}
```

Pour les plus compétiteurs, il était bien entendu possible de trouver le drapeau à grands coups de `grep` ou de `find` et un minimum de méthodologie, mais ce n'est pas aussi drôle :)
