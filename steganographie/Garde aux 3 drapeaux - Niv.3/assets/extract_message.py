from PIL import Image, ImageDraw
import base64

img = Image.open('Garde aux 3 drapeaux_repaired_and_filled.png')
img = img.convert("RGB")

binary = ""
for y in range(img.height):
  print("\rLines read : {} / {}".format(y+1, img.height), end='')
  line = ""
  for x in range(img.width):
    px = img.getpixel((x, y))
    line += str(px[0] & 0b1)

  if line.count('1'):
    binary += line

print()

b64 = ""
for i in range(0, len(binary), 8):
  if binary[i:i+8].count('1'):
    b64 += chr(int(binary[i:i+8], 2))
  
print(base64.b64decode(b64).decode("utf-8"))